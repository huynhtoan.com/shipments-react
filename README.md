The project will read the shipments from a json-server api
# Install the dependencies
`npm install`

# Start backend API
`npm run backend`

# Start frontend
`npm start`

# AC testing
- Before running the behavior tests, make sure the frontend and backend are running. 
- `npm run cypress`
## AC
- The user shall be able to:
  - See shipments in pages of 20 elements per page
  - Search by shipment id
  - Sort by different fields (e.g. id, name) in ascending/descending order
  - View the shipment information on a separate shipment details page
  - Update the shipment name (should persist when the page is reloaded)

### Cypress
- Cypress is an end to end testing framework. More detail at https://www.cypress.io/