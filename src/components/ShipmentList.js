import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Container from "@material-ui/core/Container";
import AdvancedTable from "../components/AdvancedTable";
import axios from "axios";
import shipmentConfig from "../config/shipment";
import api from '../config/api';

class ShipmentList extends Component {
  state = {
    shipments: [],
    searchString: ""
  };

  constructor() {
    super();
    this.getShipments();
  }

  getShipments = () => {
    axios
      .get([
        api.endpoint, 
        api.shipmentList,
        api.query,
        this.state.searchString
      ].join(""))
      .then(response => {
        this.setState({ shipments: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  };

  onSearchInputChange = (event) => {
    if (event.target.value) {
      this.setState({ searchString: event.target.value });
    } else {
      this.setState({ searchString: "" });
    }
    setTimeout(() => {
      this.getShipments();
    }, 0);
  }

  render() {
    return (
      <div>
        {this.state.shipments ? (
          <Container>
            <TextField
              id="searchInput"
              placeholder="Search for shipment by ID or any text"
              onChange={this.onSearchInputChange}
              fullWidth
            />
            <AdvancedTable
              headCells={shipmentConfig.headCells}
              rows={this.state.shipments}
              pathPrefix={shipmentConfig.pathPrefix}
            />
          </Container>
        ) : (
          "No shipments found"
        )}
      </div>
    );
  }
}

export default ShipmentList;
