import React, { Component } from "react";
import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Chip from "@material-ui/core/Chip";
import axios from "axios";
import api from "../config/api";
import BasicTable from "../components/BasicTable";
import shipmentViewConfig from "../config/shipmentView";

class ShipmentView extends Component {
  state = {
    shipment: {},
    shipmentName: "",
    buttonText: "Save Shipment"
  };
  constructor({ match }) {
    super();
    let shipmentId = match.params.shipmentId;
    this.getShipment(shipmentId);
  }

  getShipment = shipmentId => {
    axios
      .get(api.endpoint + api.shipmentView + shipmentId)
      .then(response => {
        this.setState({ shipment: response.data });
        this.setState({ shipmentName: this.state.shipment.name });
      })
      .catch(error => {
        console.log(error);
      });
  };

  onShipmentNameChange = (event) => {
    this.setState({ shipmentName: event.target.value });
    this.setState({ buttonText: shipmentViewConfig.saveButtonText });
  }

  saveName = () => {
    let newShipment = this.state.shipment;
    let shipmentId = newShipment.id;
    newShipment.name = this.state.shipmentName;
    axios
      .put(api.endpoint + api.shipmentView + shipmentId, newShipment)
      .then(response => {
        this.setState({ shipment: newShipment });
        this.setState({ buttonText: shipmentViewConfig.savedButtonText });
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <div>
        {this.state.shipment ? (
          <Container className="">
            <h1>
              Shipment: #{this.state.shipment.id}
              <Chip
                className={`label-${this.state.shipment.status}`}
                label={this.state.shipment.status}
              />
            </h1>
            <Button className="save_shipment" variant="contained" color="primary" onClick={this.saveName}>
              {`${this.state.buttonText}`}
            </Button>
            <Card className="detail-card">
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  <i>#{this.state.shipment.id}</i>
                </Typography>
                <Typography color="textSecondary" gutterBottom>
                  Total
                </Typography>
                <Typography variant="body1" component="h2">
                  {this.state.shipment.total}
                </Typography>
                <Typography color="textSecondary" gutterBottom>
                  Name
                </Typography>
                <TextField
                  id="shipment_name"
                  name="shipment_name"
                  fullWidth
                  value={`${this.state.shipmentName}`}
                  onChange={this.onShipmentNameChange}
                />
                <Typography color="textSecondary">Mode</Typography>
                <Typography variant="body1">
                  {this.state.shipment.mode}
                </Typography>
                <Typography color="textSecondary">Type</Typography>
                <Typography variant="body1">
                  {this.state.shipment.type}
                </Typography>
                <Typography color="textSecondary">Destination</Typography>
                <Typography variant="body1" component="p">
                  {this.state.shipment.destination}
                </Typography>
                <Typography color="textSecondary">Origin</Typography>
                <Typography variant="body1" component="p">
                  {this.state.shipment.origin}
                </Typography>
                <Typography color="textSecondary">Services</Typography>
                <Grid container spacing={1}>
                  {this.state.shipment.services
                    ? this.state.shipment.services.map((service, key) => {
                        return (
                          <Grid item key={key} xs={6} md={2}>
                            {Object.keys(service).map((item, key) => {
                              return (
                                <Chip
                                  key={key}
                                  label={`${item}: ${service[item]}`}
                                  variant="outlined"
                                />
                              );
                            })}
                          </Grid>
                        );
                      })
                    : ""}
                </Grid>
                <Typography color="textSecondary">Cargo</Typography>
                <BasicTable
                  headCells={shipmentViewConfig.headCells}
                  rows={
                    this.state.shipment.cargo ? this.state.shipment.cargo : []
                  }
                />
              </CardContent>
            </Card>
          </Container>
        ) : (
          "No shipment found for ID"
        )}
      </div>
    );
  }
}

export default ShipmentView;
