import React from 'react';
import AppBar from './components/Navbar';
import ShipmentList from "./components/ShipmentList";
import ShipmentView from './components/ShipmentView';
import './App.css';
import { BrowserRouter, Route } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppBar></AppBar>
        <Route exact={true} path="/" component={ ShipmentList }></Route>
        <Route path="/shipment/:shipmentId" component={ ShipmentView }></Route>
      </div>
    </BrowserRouter>
  );
}

export default App;
