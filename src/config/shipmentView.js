let shipment = {
  headCells: [
    { id: "type", numeric: false, disablePadding: false, label: "Type" },
    {
      id: "description",
      numeric: false,
      disablePadding: false,
      label: "Description"
    },
    { id: "volume", numeric: false, disablePadding: false, label: "Volume" }
  ],
  saveButtonText: "Save Shipment",
  savedButtonText: "Saved!"
};

export default shipment;
