let shipment = {
  headCells: [
    { id: "id", numeric: false, disablePadding: false, label: "ID" },
    {
      id: "name",
      numeric: false,
      link: true,
      disablePadding: true,
      label: "Name"
    },
    {
      id: "destination",
      numeric: false,
      disablePadding: false,
      label: "Destination",
      classes: "dn-xs"
    },
    {
      id: "mode",
      numeric: false,
      disablePadding: false,
      label: "Mode",
      classes: "dn-xs"
    },
    {
      id: "origin",
      numeric: false,
      disablePadding: false,
      label: "Origin",
      classes: "dn-xs"
    },
    {
      id: "userId",
      numeric: false,
      disablePadding: false,
      label: "User",
      classes: "dn-xs"
    }
  ],
  pathPrefix: "shipment"
};

export default shipment;
